/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package javafxmusicplayer;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXSlider;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Paint;
import javafx.stage.DirectoryChooser;
import javafx.util.Duration;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import de.jensd.fx.glyphs.materialicons.MaterialIcon;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author user
 */
public class FXMusicPlayerController implements Initializable {

    private File cacheFolder, songLibrary, playHistory, 
            selectedDirectory, likedSongLibrary, songMetaData;//because there's a concurrency problem when mediaplayer
    //is not getting synced up with the loading of songs.
    private ObservableList<MusicProperty> songs, likedSongs, songsCopy, searchSongList;
    private Media media;
    private final DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("hh:mm:ss a EEE, dd MMM yyyy");
    private MediaPlayer mediaPlayer;
    private DirectoryChooser folderPrompt;
    private boolean isPlaying = false, isPaused = false, repeat = false, shuffle = false, allTracks = true;
    private MusicProperty music;
    private int index = -1;
    private final Random generator = new Random();
    private BufferedWriter writer;
    private Scanner scanner;
    
    @FXML
    private TableView<MusicProperty> songTable;
    
    @FXML
    private JFXButton btnAllTracks, btnLikedSongs, btnNext, btnPausePlay,
            btnPlayHistory, btnPrev, btnRepeat, btnShuffle, btnSelectFolder
            ,btnLikedSong;
    
    @FXML
    private JFXListView listOfPlaylist;
    
    @FXML
    private FontAwesomeIconView pausePlayIcon, btnRepeatIcon,
    likedSong, volumeIcon;
    
    @FXML
    private MaterialDesignIconView btnShuffleIcon;
    
    @FXML
    private Slider volumeSlider, songProgressBar;
    
    @FXML
    private Label lblSongName, lblCurrentSelection, lblTotalTime, lblCurrentProgress;
    
    
    @FXML
    private ContextMenu tableContextMenu;
    
    @FXML
    private TextField txtSearchSong;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        initProgram();
        
    }
    
    private void savePlayHistoryData(){
    
        music.setLastPlayed(timeFormat.format(LocalDateTime.now()));
        for(MusicProperty song: songs){
        
            if(song == music){
            
                song.setLastPlayed(music.getLastPlayed());
                break;
            }
        }
       
    }
    
    private void addNewSong(File[] files){
        
        HashSet<String> compareSet1 = new HashSet();
        HashSet<String> compareSet2 = new HashSet();
        for(File file: files){
        
            compareSet1.add(file.getName());
        }
        
        for(MusicProperty music: songs){
        
            compareSet2.add(music.getFileName());
        }
        
        compareSet1.removeAll(compareSet2);
        if(!compareSet1.isEmpty()){
        
            for(String fileName : compareSet1){
            
                MusicProperty musicData = new MusicProperty();
                musicData.setFileName(fileName);
                musicData.setFilePath(files[0].getParent());
                musicData.setDateAdded(timeFormat.format(LocalDateTime.now()));
                musicData.setDuration("");
                songs.add(musicData);
                if(allTracks){
                
                    songsCopy.clear();
                    songsCopy.addAll(songs);
                }
            }
        }
    }

    @FXML
    private void searchSong(KeyEvent event){
    
       String songName = txtSearchSong.getText().trim();
       
       if(songsCopy == null)
           return;
       if(!songName.isEmpty()){
       
           if(songs != null || !songs.isEmpty()){
    
           if(searchSongList == null)
               searchSongList = FXCollections.observableArrayList();
            searchSongList.clear();
            for(MusicProperty musicData : songs){
       
                if(musicData.getFileName().contains(songName)){
          
                    searchSongList.add(musicData);
                }
            }
            
            songsCopy.clear();
            songsCopy.addAll(searchSongList);
            reloadTable(songsCopy);
            }
           
       }else {
       
           songsCopy.clear();
           songsCopy.addAll(songs);
           
       }
       
       
    }
    
    private void scanSongs(){

        try {
            Scanner scanSongs = new Scanner(songLibrary);
            
            if(scanSongs.hasNext()){
            
                String filePath = scanSongs.nextLine();
                File path = new File(filePath);
                
                File[] listFiles = path.listFiles((File dir, String name) -> name.toLowerCase().endsWith(".mp3"));
                if(listFiles != null){
                
                    if(listFiles.length != songs.size()){
                
                    if(listFiles.length > songs.size()){

                        addNewSong(listFiles);
                    }
                    }
                addNewSong(listFiles);
                
                }
                
            }
        } catch (Exception ex) {
            Logger.getLogger(FXMusicPlayerController.class.getName()).log(Level.SEVERE, null, ex);
            this.saveMusicMetadata();
        }
    }
    private void initProgram(){
        
        cacheFolder = new File(String.format("%s/%s", System.getenv("USERPROFILE"),".fxMusicPlayerCache"));
        cacheFolder.mkdirs();
        songLibrary = new File(String.format("%s/%s", cacheFolder.getAbsolutePath(), "SongLibrary.txt"));
        likedSongLibrary = new File(String.format("%s/%s", cacheFolder.getAbsolutePath(), "LikedSongs.txt"));
        playHistory = new File(String.format("%s/%s", cacheFolder.getAbsolutePath(), "PlayHistory.txt"));
        songMetaData = new File(String.format("%s/%s", cacheFolder.getAbsolutePath(), "SongMetadata.txt"));
        try {
            songLibrary.createNewFile();
            likedSongLibrary.createNewFile();
            playHistory.createNewFile();
            songMetaData.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(FXMusicPlayerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        retrieveMusicMetadata();
        TableColumn<MusicProperty, String> musicName = new TableColumn<>("Song Name");
        TableColumn<MusicProperty, String> musicDurationColumn = new TableColumn<>("Duration");
        TableColumn<MusicProperty, String> dateAddedColumn = new TableColumn<>("Date Added");
        musicName.setCellValueFactory(new PropertyValueFactory<>("fileName"));
        musicDurationColumn.setCellValueFactory(new PropertyValueFactory<>("duration"));
        dateAddedColumn.setCellValueFactory(new PropertyValueFactory<>("dateAdded"));
        songTable.getColumns().clear();
        songTable.getColumns().addAll(musicName, musicDurationColumn, dateAddedColumn);
        volumeSlider.valueProperty().addListener((ObservableValue<? extends Number> e, Number a, Number b) -> {
            if(volumeSlider.getValue() == 0){
                
                volumeIcon.setIcon(FontAwesomeIcon.VOLUME_OFF);
            }else if(volumeSlider.getValue() <= 0.5){
                
                volumeIcon.setIcon(FontAwesomeIcon.VOLUME_DOWN);
            }else {
                
                volumeIcon.setIcon(FontAwesomeIcon.VOLUME_UP);
            }
        });
        
        Thread scanSong = new Thread(){
        
            @Override
            public void run(){
            
                try {
                    
                    while(true){
                        scanSongs();
                        Thread.sleep(2000);
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(FXMusicPlayerController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        songProgressBar.setOnMouseReleased((e) -> {
            
                mediaPlayer.seek(Duration.seconds(songProgressBar.getValue()));
            });
        scanSong.setDaemon(true);
        scanSong.start();
        btnLikedSong.setTooltip(new Tooltip("Press twice to add it to like songs"));
        btnRepeat.setTooltip(new Tooltip("Click to Repeat"));
        btnNext.setTooltip(new Tooltip("Next song"));
        btnPrev.setTooltip(new Tooltip("Previous song"));
        btnPausePlay.setTooltip(new Tooltip("Play"));
        btnShuffle.setTooltip(new Tooltip("Press to shuffle"));
    }
    
    @FXML
    private void selectLikedSongs(){
    
        songTable.getItems().clear();
        retrieveLikedSong();
        lblCurrentSelection.setText("Liked Songs");
        if(!likedSongs.isEmpty()){
        
            songsCopy.clear();
            songsCopy.addAll(likedSongs);
             reloadTable(songsCopy);
             songTable.setPlaceholder(null);
        }else {
        
            songTable.setPlaceholder(new Label("Liked Song is empty."));
        }
        allTracks = false;
    }
    
    @FXML
    private void saveMusicMetadata(){
    
       saveMusicData();
        
    }
    
    private void saveMusicData(){
    
        if(!songs.isEmpty()){
                try {
                    writer = new BufferedWriter(new FileWriter(this.songMetaData));
                    songs.clear();
                    songs.addAll(songsCopy);
                    songsCopy.clear();
                    songsCopy.addAll(songs);
                    songTable.getSelectionModel().select(index);
                    for(MusicProperty data : songs){
            
                        writer.write(String.format("%s\n%s\n%s\n%s\n%s\n%s\n", data.getFilePath(),
                        data.getFileName(),data.getLikedSong(),data.getDateAdded(),
                        data.getDuration(), data.getLastPlayed()));
                        writer.flush();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(FXMusicPlayerController.class.getName()).log(Level.SEVERE, null, ex);
                }finally {
                
                    try{
                        writer.close();        
                    }catch(IOException e){}
                }            
        }
    }
    
    private boolean retrieveMusicMetadata(){
        String filePath;
        String fileName;
        String likedSongData;
        String dateAdded;
        String duration;
        String lastPlayed;
        MusicProperty data;
        try {
 
            scanner = new Scanner(this.songMetaData);
            if(!scanner.hasNextLine())
                return false;
            songs = FXCollections.observableArrayList();
            while(scanner.hasNextLine()){
                try{
                    data = new MusicProperty();
                    filePath = scanner.nextLine();
                    fileName = scanner.nextLine();
                    likedSongData = scanner.nextLine().trim();
                    dateAdded = scanner.nextLine();
                    duration = scanner.nextLine();
                    lastPlayed = scanner.nextLine();
                    data.setFilePath(filePath);
                    data.setFileName(fileName);
                    data.setLikedSong(Boolean.parseBoolean(likedSongData));
                    data.setDateAdded(dateAdded);
                    data.setDuration(duration);
                    data.setLastPlayed(lastPlayed);
                    songs.add(data);
                }catch(Exception e){
                    return false;
                }
            }
            songsCopy = FXCollections.observableArrayList(songs);
            reloadTable(songsCopy);
        } catch (FileNotFoundException ex) {
            return false;
        }finally {
        
            scanner.close();
        }
        
        return true;
    }
    @FXML
    private void menuSaveToLikedSong(){
    
        if(index > -1 && index < songTable.getItems().size()){
        
            savedLikedSong(true);
        }
    }
    
    @FXML
    private void menuSaveToPlaylist(){
    
        
        
    }
    
    @FXML
    private void selectAllTracks(){
        songTable.getItems().clear();
        lblCurrentSelection.setText("All Tracks");
        if(songs != null){
        
            if(!songs.isEmpty()){
        
             songsCopy.clear();
             songsCopy.addAll(songs);
             reloadTable(songsCopy);
             songTable.setPlaceholder(null);
            }else {
        
                songTable.setPlaceholder(new Label("Music library is empty."));
            }
        }
        
        allTracks = true;
    }
    
    @FXML
    private void selectPlayHistory(){
    
        songTable.getItems().clear();
        if(songsCopy != null){
            
            songsCopy.clear();
            for(MusicProperty song : songs){
            
                if(!song.getLastPlayed().equals("null")){
                
                    songsCopy.add(song);
                }
            }
            Collections.sort(songsCopy, new Comparator<MusicProperty>(){
                @Override
                public int compare(MusicProperty o1, MusicProperty o2) {
                    if(o1.getLastPlayed() != null && o2.getLastPlayed() != null){
                    
                        return o1.getLastPlayed().compareTo(o2.getLastPlayed());
                    }
                    return 0;
                }
            }.reversed());
        }

        allTracks = false;
    }
    
    @FXML
    private void selectFolder(){
        
        try{

            folderPrompt = new DirectoryChooser();
            folderPrompt.setTitle("Choose Music folder");
            folderPrompt.setInitialDirectory(new File(System.getenv("USERPROFILE")));
            selectedDirectory = folderPrompt.showDialog(null);
            loadMusic(selectedDirectory);
            saveMusicFolderDirectory();
        }catch(Exception e){
        
            System.out.println("Error encounter.");
        }
        
        
    }
    
    private void saveMusicFolderDirectory(){
    
        try {
            //next feature: can add multiple music folder.
            writer = new BufferedWriter(new FileWriter(songLibrary));
            writer.write(selectedDirectory.getAbsoluteFile().getAbsolutePath());
            writer.flush();
        } catch (IOException ex) {
            
        }
    }
    
    private void savedLikedSong(boolean overwrite){
    
        try {
            writer = new BufferedWriter(new FileWriter(likedSongLibrary,overwrite));
            writer.write(music.getFilePath()+"\\\n"+music.getFileName()+"\n");
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(FXMusicPlayerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    private void retrieveLikedSong(){
    
        likedSongs = FXCollections.observableArrayList();
        if(songs != null){
        
            for(MusicProperty data: songs){
        
            if(data.getLikedSong()){
            
                likedSongs.add(data);
            }
        }
        }
        
    }
    
    private void reloadTable(ObservableList<MusicProperty> songs){
        
        songTable.setItems(songs);
        this.isPlaying = false;
        music = null;
    }
    
    private void highlightSelectedSong(){
    
        songTable.getSelectionModel().select(index);
    }
    
    @FXML
    private void dgvMouseClicked(MouseEvent event){
    
      music = songTable.getSelectionModel().getSelectedItem();
      index = songTable.getSelectionModel().getSelectedIndex();
      
    if(songTable.getSelectionModel().getSelectedItem() != null){
            
        if(music.getLikedSong()){
      
          likedSong.setFill(Paint.valueOf("RED"));
        }else {
      
          likedSong.setFill(Paint.valueOf("WHITE"));
        }
        if(event.getClickCount() == 2 && music != null){
        
        this.isPlaying = true;
        this.isPaused = false;
        loadMedia();
        playMedia();
    
        }    
        
    }
    
    }
    
    private void loadMedia(){
    
        highlightSelectedSong();
        if(this.isPlaying && mediaPlayer != null)
            mediaPlayer.stop();
        media = new Media(new File(String.format("%s/%s",
                music.getFilePath(),music.getFileName())).toURI().toString());
        lblSongName.setText(music.getFileName());
        mediaPlayer = new MediaPlayer(media);
       
    }
    
    private void playMedia(){
        
        pausePlayIcon.setIcon(FontAwesomeIcon.PAUSE);
        btnPausePlay.setTooltip(new Tooltip("Pause"));
        mediaPlayer.volumeProperty().bind(volumeSlider.valueProperty());
        mediaPlayer.play();
        savePlayHistoryData();
        mediaPlayer.setOnEndOfMedia(() -> {
            
            if(this.repeat){
            
                mediaPlayer.seek(Duration.ZERO);
            }else if(index <= songs.size() -1){
                
                if(!shuffle){
                
                    index++;
                    if(index >= songs.size())
                        index = 0;
                }
                else
                    index = generator.nextInt(songs.size()-1);
                music = songs.get(index);
                loadMedia();
                playMedia();
            }
        });

        mediaPlayer.setOnReady(() -> {
        
            
            int seconds = (int)mediaPlayer.getTotalDuration().toSeconds();
            int minutes = seconds/60;
            int remainingSeconds = seconds%60;
            if(music.getDuration().isEmpty()){
                music.setDuration(String.format("%02d:%02d", minutes,remainingSeconds));
                saveMusicData();
            }

            startProgress();
            lblTotalTime.setText(music.getDuration());
        });

    }
    
    @FXML
    private void nextMedia(){
    
        if(!songsCopy.isEmpty()){
        
            if(index < songsCopy.size() -1){
            
                if(!shuffle)
                    index++;
                else
                    index = generator.nextInt(songsCopy.size() -1);
                
            }else {
                index = 0;
            }
                music = songsCopy.get(index);
                this.isPlaying = true;
                loadMedia();
                playMedia();
        }
    
    }
    
    @FXML
    private void prevMedia(){
    
         if(!songs.isEmpty()){
        
            if(index > 0){
            
                index--;
                
            }else {
                index = songs.size() - 1;
            }
                music = songs.get(index);
                this.isPlaying = true;
                loadMedia();
                playMedia();
        }
    
    }

    @FXML
    private void repeatSong(){
    
       repeat = !repeat;
       if(repeat){
       
           btnRepeatIcon.setIcon(FontAwesomeIcon.REFRESH);
           btnRepeat.setTooltip(new Tooltip("Click to no Repeat"));
           
       }else {
       
            btnRepeatIcon.setIcon(FontAwesomeIcon.REPEAT);
             btnRepeat.setTooltip(new Tooltip("Click to Repeat"));
       }
    }
    
    @FXML
    private void shuffleMedia(){
    
        this.shuffle = !shuffle;

        if(!songs.isEmpty()){
        
            if(shuffle){
                index = generator.nextInt(songs.size() -1);
                btnShuffleIcon.setIcon(MaterialDesignIcon.SHUFFLE);
                btnShuffle.setTooltip(new Tooltip("Press to off shuffle"));
            }else {
            
                btnShuffleIcon.setIcon(MaterialDesignIcon.SHUFFLE_DISABLED);
                btnShuffle.setTooltip(new Tooltip("Press to shuffle"));

            }
        }
    }
    @FXML
    private void pausePlayMedia(){
        
        index = songTable.getSelectionModel().getSelectedIndex();
        music = songTable.getSelectionModel().getSelectedItem();
        if(!this.isPlaying && music != null){
        
            this.isPlaying = true;
            loadMedia();
        }else if(index < 0 && !songTable.getItems().isEmpty() && !isPaused && !isPlaying) {
        
            if(!isPlaying && !this.isPaused){
            
                index = 0;
                music = songs.get(index);
                this.isPlaying = true;
                loadMedia();
                playMedia();
            }
        }else 
        {
            if(this.isPaused){
                this.isPaused = false;
                playMedia();
            }else if(music != null) {
                
                this.isPaused = true;
                pauseMedia();
            }
        }
    }
    
    @FXML
    private void onLikedClick(MouseEvent event){
    
        int musicIndex = songTable.getSelectionModel().getSelectedIndex();
        System.out.println(musicIndex);
        if(musicIndex >= 0) {
        
            if(event.getClickCount() == 1){
            boolean isLiked = music.getLikedSong();
            if(isLiked){
            
                likedSong.setFill(Paint.valueOf("WHITE"));
            }else {
            
                likedSong.setFill(Paint.valueOf("RED"));
                 saveMusicData();
            }
            music.setLikedSong(!isLiked);
            System.out.println(allTracks);
            if(allTracks)
                saveMusicData();
        }
            
        }
        
    }
    

    private void pauseMedia(){
    
        pausePlayIcon.setIcon(FontAwesomeIcon.PLAY);
        btnPausePlay.setTooltip(new Tooltip("Play"));
        mediaPlayer.pause();
    }
    
    private void startProgress(){
    
        try{
  
            songProgressBar.maxProperty().bind(
                Bindings.createDoubleBinding(
                () -> mediaPlayer.getTotalDuration().toSeconds(),
                mediaPlayer.totalDurationProperty())
            );
            
            mediaPlayer.currentTimeProperty().addListener(new ChangeListener(){
                @Override
                public void changed(ObservableValue ov, Object t, Object t1) {
                   
                   songProgressBar.setValue(mediaPlayer.getCurrentTime().toSeconds());
                }
            });
            mediaPlayer.currentTimeProperty().addListener((Observable o) -> {
                if(songProgressBar.isValueChanging()){
                    
                    mediaPlayer.seek(Duration.seconds(songProgressBar.getValue()));
                }
            });
        
            lblCurrentProgress.textProperty().bind(Bindings.createStringBinding(() -> {
           
                Duration current = mediaPlayer.getCurrentTime();
                int currentTime = (int) current.toSeconds() % 60;
                return String.format("%02d:%02d", (int)current.toSeconds()/60,currentTime);
            },      mediaPlayer.currentTimeProperty(),
                    mediaPlayer.cycleCountProperty()
                    
                    ));
        }catch(Exception e){
        
        }
        
    
    }
    
    private void loadMusic(File selectedDirectory){
    
        File[] listFiles = selectedDirectory.listFiles(new FilenameFilter(){
            @Override
            public boolean accept(File dir, String name) {

                return name.toLowerCase().endsWith(".mp3");
            }
        });

        songs = FXCollections.observableArrayList();
        for(File file : listFiles){
            
            songs.add(new MusicProperty(file.getName(), file.getAbsoluteFile().getParent(), false, timeFormat.format(LocalDateTime.now()), ""));   
        }
        songsCopy = FXCollections.observableArrayList(songs);
        reloadTable(songsCopy);
    }
    
}
