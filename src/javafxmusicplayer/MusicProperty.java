/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package javafxmusicplayer;

/**
 *
 * @author user
 */
public class MusicProperty {
    
    private String fileName;
    private String filePath;
    private boolean likedSong;
    private String dateAdded;
    private String duration;
    private String lastPlayed;
    
    public MusicProperty(String fileName, String filePath, boolean isLiked, String dateAdd, String duration){
    
        this.fileName = fileName;
        this.filePath = filePath;
        this.likedSong = isLiked;
        this.dateAdded = dateAdd;
        this.duration = duration;
    }
    
    public MusicProperty(){
    
        
    }
    public void setFileName(String fileName){
    
        this.fileName = fileName;
    }
    
    public String getFileName(){
    
        return this.fileName;
    }
    
    public void setFilePath(String filePath){
    
        this.filePath = filePath;
    }
    
    public String getFilePath(){
    
    
        return filePath;
    }
    
    public void setLikedSong(boolean value){
    
        this.likedSong = value;
    }
    
    public boolean getLikedSong(){
    
        return likedSong;
    }
    
    public void setDateAdded(String dateTime){
    
        this.dateAdded = dateTime;
    }
    
    public String getDateAdded(){
    
        return this.dateAdded;
    }
    
    public void setDuration(String duration){
    
    
        this.duration = duration;
    }
    
    public String getDuration(){

        return duration;
    }
    
    public void setLastPlayed(String lastPlayed){
    
        this.lastPlayed = lastPlayed;
    }
   
    public String getLastPlayed(){
    
        return this.lastPlayed;
    }
}
