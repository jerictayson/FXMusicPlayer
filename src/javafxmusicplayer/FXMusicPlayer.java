/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package javafxmusicplayer;

import java.io.File;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author user
 */
public class FXMusicPlayer extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        File UIFile = new File("MusicPlayerTemplate.fxml");
        Parent pane = FXMLLoader.load(UIFile.toURI().toURL());
        Scene myScene = new Scene(pane);
        stage.setScene(myScene);
        stage.setTitle("FX Music Player");
        stage.show();
    }
    
}
