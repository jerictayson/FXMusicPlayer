# FXMusicPlayer

**Note this app is not fully finished and some functionality is not implemented yet.**

## Pre-requisite to build this app
* JavaFX & Java 8 [(Zulu Build)](https://www.azul.com/)
* [Netbeans](https://netbeans.apache.org/)
* Libraries Needed:
	* [JFoenix](https://github.com/sshahine/JFoenix)
	* [FontAwesome](https://bitbucket.org/Jerady/fontawesomefx/downloads/)
## Screenshots

<img src="./screenshots/pic1.jpg" alt="AllTracksView">

<img src="./screenshots/pic2.jpg" alt="LikedSongsView">

## Binaries
**[Windows Build](https://mega.nz/folder/Djh3GD6Q#8aS2lQ2D4cS11wJO9Qpeog)**
